import styles from '../styles/name.module.scss'

const Name = () => {
	return <h1 className={styles.name} >Faith Kiddo</h1>;
};

export default Name;
