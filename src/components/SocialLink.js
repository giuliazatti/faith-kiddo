import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function SocialLink(props) {
	let icon = "";

	if (props.icon) {
		icon = <FontAwesomeIcon icon={props.icon}></FontAwesomeIcon>;
	} else {
		icon = <img src={props.img} alt={`Faith Kiddo - ${props.socialName} Link`} />;
	}

	return (
		<a href={props.href} target="_blank" rel="noreferrer">
			{icon}
		</a>
	);
}

export default SocialLink;
