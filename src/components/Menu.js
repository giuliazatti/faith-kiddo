import { Link } from "react-router-dom";
import imgPress from "../images/IMG_1911.jpg";
import imgExplore from "../images/IMG_1985.jpg";
import styles from "../styles/menu.module.scss";

const Menu = () => {
	return (
		<section className={`${styles.menuWrapper} flex-center`}>
			<nav>
				<ul className="flex-center">
					<li className="flex-center">
						<img src={imgExplore} alt="Esplora l'EP" />
						<Link to="/explore">ESPLORA L'EP</Link>
					</li>
					<li className="flex-center">
						<img src={imgPress} alt="Dicono di me" />
						<Link to="/press">DICONO DI ME</Link>
					</li>
				</ul>
			</nav>
		</section>
	);
};

export default Menu;
